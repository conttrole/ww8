#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ww8_library.h>

#define DEBUG 0

Ww8_Library ww8Obj;

/* Configurando as credenciais da rede a qual o ESP se conectará. */
char* rede = "MINHAREDETAMANHOMAXIMO1";
char* senha = "MINHASENHATAMANHOMAXIMO";
char* ip = "000.000.000.000";
char* gateway = "255.255.255.255";
char* title_redes = "0 Redes Encontradas ";
char* redes[10] = {"Rede 1                        ",
                    "Rede 2                        ",
                    "Rede 3                        ",
                    "Rede 4                        ",
                    "Rede 5                        ",
                    "Rede 6                        ",
                    "Rede 7                        ",
                    "Rede 8                        ",
                    "Rede 9                        ",
                    "Rede 10                       "};

bool wifi[2] = {false, true};
bool checkeds[8] = {false, false, false, false, false, false, false, false};
int pinos[8] = {1, 3, 5, 4, 16, 14, 12, 13};
int rele_type[8] = {0, 0, 0, 0, 0, 0, 0, 0};
char* reles[8] = {"Rele 1                        ",
                  "Rele 2                        ",
                  "Rele 3                        ",
                  "Rele 4                        ",
                  "Rele 5                        ",
                  "Rele 6                        ",
                  "Rele 7                        ",
                  "Rele 8                        "};

/* Iniciando servidor. */
ESP8266WebServer server(80);

/* Funções para capturar paginas e imprimir no browser */
void index_func();
void configurar_wifi_func();
void servidor_func();
void ap_func();
void ajuda_func();
void acionar_dispositivo_func();
void configurar_dispositivo_func();
void contato_func();

/* Funções úteis para conversões e configurações */
void parseBytes(char* str, char sep, byte* bytes, int maxBytes, int base);
String ipToCharArray(IPAddress myIP);
void conectar_ap(char* ssid, char* password);
boolean conectar_servidor(char* ssid, char* password, char* ip_string, char* gateway_string);
boolean conectar_servidor_sem_ip(char* ssid, char* password);
void atualizar_servidor();
void pesquisar_wifi();
void sendPage(String page);


void setup() {
  String aux;
  for(int i = 0; i < 8; i++){
    aux = String(reles[i]);
    aux.trim();
    aux.toCharArray(reles[i], aux.length() + 1);
  }

  for(int i = 0; i < 10; i++){
    aux = String(redes[i]);
    aux.trim();
    aux.toCharArray(redes[i], aux.length() + 1);
  }

  rede = "rede_ww8";
  senha = "senha_ww8";
  //gateway = "192.168.0.1";
  
  /* Aguarda 1s para fazer o start do ESP */
  delay(1000);
  
  /* Iniciando comunicação Serial*/
  if (DEBUG) Serial.begin(115200);
  
  //Iniciando as portas de saída 
  for(int i = 2; i < 8; i++){
    pinMode(pinos[i], OUTPUT);
  }
  
  /* Imprimindo dados para Debug */
  if (DEBUG) Serial.println();
  if (DEBUG) Serial.print("Configuring server...");

  /* Criando rede */
  conectar_ap(rede, senha);
  //conectar_servidor_sem_ip(rede, senha);
  /* DEBUG */
  if (DEBUG) Serial.println("HTTP server started");
}

void loop() {
    /* Servidor continua respondendo o cliente */
    server.handleClient();
}

PROGMEM const char pre_page[] = "<!DOCTYPE html>\r\n"
    "   <html>\r\n"
    "    <head>\r\n"
    "     <meta content='text/html; charset=utf-8' http-equiv='Content-Type' />\r\n"
    "     <title>WW8 Conttrole</title>\r\n"
    "     <style type='text/css'>\r\n"
    "    html{\r\n"
    "   position: relative;\r\n"
    "   min-height: 100%;\r\n"
    "   }\r\n"
    "   \r\n"
    "   body {\r\n"
    "           margin:0 0 80px;\r\n"
    "           padding:0;\r\n"
    "           background: #FFFFF;\r\n"
    "           font-family: Sans-Serif;\r\n"
    "           line-height: 1.5em;\r\n"
    "         }\r\n"
    " \r\n"
    "         table{\r\n"
    "     position: relative;\r\n"
    "            border: 0; padding: 1px 1px 1px 1px;\r\n"
    "     top: 15px;\r\n"
    "     width: 30%;\r\n"
    "     left: -370px;\r\n"
    "     font-size: 30px;\r\n"
    "     //align: center;\r\n"
    "     }\r\n"
    "     table h2{\r\n"
    "     font-size: 30px;\r\n"
    "     }\r\n"
    "         \r\n"
    "    #header {\r\n"
    "      position: fixed\r\n"
    "      top: 0;\r\n"
    "           background: linear-gradient(to bottom, rgba(19,19,19,1) 0%, rgba(14,37,65,1) 27%, rgba(5,67,143,0.99) 72%, rgba(0,86,191,0.99) 100%);\r\n"
    "           height: 200px;\r\n"
    "           color: #FFF;\r\n"
    "         }\r\n"
    "        \r\n"
    "   #header h1 {\r\n"
    "           margin: 0;\r\n"
    "           padding-top: 70px;\r\n"
    "           position: absolute;\r\n"
    "           color: #FFF;\r\n"
    "           text-align: center\r\n"
    "         }\r\n"
    "         \r\n"
    "         #botao{ text-align: center }\r\n"
    "         \r\n"
    "    .botaoEnviar\r\n"
    "         {\r\n"
    "           width: 180px;\r\n"
    "           text-align: center;\r\n"
    "           padding: 10px 5px;\r\n"
    "           border: 1px solid #ccc;\r\n"
    "           border-radius: 2px;\r\n"
    "           color: #1C1C1C;\r\n"
    "           background-color: #000\r\n"
    "           font-size: 18px;\r\n"
    "         }\r\n"
    "         .confWifi{\r\n"
    "           text-align: center;\r\n"
    "         }\r\n"
    "        \r\n"
    "         #main{\r\n"
    "         margin:10px 10px 10px 10px;}\r\n"
    "         \r\n"
    "    #footer{\r\n"
    "           clear: left;\r\n"
    "           width: 100%;\r\n"
    "           background: rgba(11,15,20,1);\r\n"
    "           text-align: center;\r\n"
    "           padding: 5px 0;\r\n"
    "           position:absolute;\r\n"
    "           bottom:0; height:60px;\r\n"
    "         }\r\n"
    "         \r\n"
    "    #wrapper{\r\n"
    "           overflow: none; margin:0;\r\n"
    "         }\r\n"
    "         \r\n"
    "    #innertube {\r\n"
    "           background: rgba(11,15,20,1);\r\n"
    "           color: #0000;\r\n"
    "           margin: 25px;\r\n"
    "           margin-top: 0;\r\n"
    "         }\r\n"
    "         \r\n"
    "    #p{\r\n"
    "            color: #FFFF;\r\n"
    "         }\r\n"
    "         \r\n"
    "    p.cont{\r\n"
    "           color: #000;\r\n"
    "           width: 100%;\r\n"
    "           text-align: center;\r\n"
    "           font-size: 20px;\r\n"
    "         }\r\n"
    "         p.ww8{\r\n"
    "           padding-top: 50px;\r\n"
    "           position: absolute;\r\n"
    "           color: #FFF;\r\n"
    "           font-size: 50px;\r\n"
    "           width: 100%;\r\n"
    "           text-align: center;\r\n"
    "         }\r\n"
    "    \r\n"
    "      .switch {\r\n"
    "       position: relative;\r\n"
    "       display: inline-block;\r\n"
    "       width: 70px;\r\n"
    "       height: 40px;\r\n"
    "     }\r\n"
    "\r\n"
    "     /* Hide default HTML checkbox */\r\n"
    "     .switch input {display:none;}\r\n"
    "\r\n"
    "     /* The slider */\r\n"
    "     .slider {\r\n"
    "       position: absolute;\r\n"
    "       cursor: pointer;\r\n"
    "       top: 0;\r\n"
    "       left: 0;\r\n"
    "       right: 0;\r\n"
    "       bottom: 0;\r\n"
    "       background-color: #ccc;\r\n"
    "       -webkit-transition: .4s;\r\n"
    "       transition: .4s;\r\n"
    "     }\r\n"
    "\r\n"
    "     .slider:before {\r\n"
    "       position: absolute;\r\n"
    "       content: \"\";\r\n"
    "       height: 30px;\r\n"
    "       width: 30px;\r\n"
    "       left: 4px;\r\n"
    "       bottom: 6px;\r\n"
    "       background-color: white;\r\n"
    "       -webkit-transition: .4s;\r\n"
    "       transition: .4s;\r\n"
    "     }\r\n"
    "\r\n"
    "     input:checked + .slider {\r\n"
    "       background-color: #2196F3;\r\n"
    "     }\r\n"
    "\r\n"
    "     input:focus + .slider {\r\n"
    "       box-shadow: 0 0 1px #2196F3;\r\n"
    "     }\r\n"
    "\r\n"
    "     input:checked + .slider:before {\r\n"
    "       -webkit-transform: translateX(26px);\r\n"
    "       -ms-transform: translateX(26px);\r\n"
    "       transform: translateX(30px);\r\n"
    "     }\r\n"
    "\r\n"
    "     /* Rounded sliders */\r\n"
    "     .slider.round {\r\n"
    "       border-radius: 50px;\r\n"
    "     }\r\n"
    "\r\n"
    "     .slider.round:before {\r\n"
    "       border-radius: 50%;\r\n"
    "     }\r\n"
    "\r\n"
    "     /* lists */\r\n"
    "     .list-full, .list-full li,\r\n"
    "     .list-auto, .list-auto li { width: 100%; float: left; display: block; position: relative; }\r\n"
    "     .list-auto, .list-auto li {width: auto;} \r\n"
    "     .\r\n"
    "     /* floats */\r\n"
    "     .float-l { float: left; }\r\n"
    "     .float-r { float: left; top: 100;}\r\n"
    "\r\n"
    "     body {\r\n"
    "       font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\r\n"
    "       color: #333;\r\n"
    "       text-shadow: 0 1px 0 #fff;\r\n"
    "     }\r\n"
    "     \r\n"
    "     header {\r\n"
    "       min-height: 60px;\r\n"
    "       position: relative;\r\n"
    "       top: 0;\r\n"
    "       right: 0;\r\n"
    "       left: 0;\r\n"
    "       border-bottom: 0px solid #ccc;\r\n"
    "       background: #fff;\r\n"
    "       height: 50px;\r\n"
    "       z-index: 2;\r\n"
    "     }\r\n"
    "\r\n"
    "     header ul {\r\n"
    "     padding: 0px 15px 0 0;\r\n"
    "     }\r\n"
    "\r\n"
    "     header li {\r\n"
    "     border-left: 1px solid #ccc; padding: 0px 35px 0 0; \r\n"
    "     }\r\n"
    "\r\n"
    "     header li:first-child {\r\n"
    "       border: none;\r\n"
    "     }\r\n"
    "\r\n"
    "     header li a {\r\n"
    "       display: block;\r\n"
    "       padding: 0px 20px;\r\n"
    "       color: #000;\r\n"
    "       font-size: 30px;\r\n"
    "       line-height: 30px;\r\n"
    "       left:0px;\r\n"
    "       text-decoration: none;\r\n"
    "       -webkit-transition: all 300ms ease;\r\n"
    "       transition: all 300ms ease;\r\n"
    "     }\r\n"
    "\r\n"
    "     header li a:hover {\r\n"
    "       color: #0000CD;\r\n"
    "       -moz-box-shadow:0 4px 10px 0 #000; \r\n"
    "       -webkit-box-shadow:0 4px 10px 0 #000;\r\n"
    "     }\r\n"
    "     \r\n"
    "     header ul li  ul{\r\n"
    "       position:absolute; \r\n"
    "       top:33px;\r\n"
    "       line-height: 40px;  \r\n"
    "       left:0px;\r\n"
    "       background-color:#fff; \r\n"
    "       display:none;\r\n"
    "       width:370px;\r\n"
    "     }\r\n"
    "     \r\n"
    "     header ul li:hover ul{display: block;}\r\n"
    "     \r\n"
    "\r\n"
    "     input#control-nav {\r\n"
    "       visibility: hidden;\r\n"
    "       position: absolute;\r\n"
    "       left: -9999px;\r\n"
    "       opacity: 0;\r\n"
    "     }\r\n"
    "\r\n"
    "     \r\n"
    "     .tooltip {\r\n"
    "       position: relative;\r\n"
    "       display: inline-block;\r\n"
    "     }\r\n"
    "\r\n"
    "     .tooltip .tooltiptext {\r\n"
    "       visibility: hidden;\r\n"
    "       width: 40%;\r\n"
    "       background-color: #7B68EE;\r\n"
    "       color: #fff;\r\n"
    "       opacity: 50%;\r\n"
    "       text-align: center;\r\n"
    "       border-radius: 6px;\r\n"
    "       padding: 100px 0;\r\n"
    "\r\n"
    "       /* Position the tooltip */\r\n"
    "       position: fixed;\r\n"
    "       top: 200px;\r\n"
    "       left: 60%;\r\n"
    "       z-index: 1;\r\n"
    "     }\r\n"
    "\r\n"
    "     .tooltip:hover .tooltiptext {\r\n"
    "       visibility: visible;\r\n"
    "     }\r\n"
    "     \r\n"
    "\r\n"
    "     @media screen and (max-width: 1000px) {\r\n"
    "       header nav {\r\n"
    "       position: fixed;\r\n"
    "       top: 0;\r\n"
    "       bottom: 0;\r\n"
    "       right: 0;\r\n"
    "       width: 350px;\r\n"
    "       border-left: 1px solid #ccc;\r\n"
    "       background: #CFD8DC;\r\n"
    "       overflow-x: auto;\r\n"
    "       z-index: 2;\r\n"
    "       -webkit-transition: all 300ms ease;\r\n"
    "       transition: all 300ms ease;\r\n"
    "       transform: translate(100%, 0);\r\n"
    "       }\r\n"
    "\r\n"
    "       header ul.list-auto {\r\n"
    "       padding: 0;\r\n"
    "       }\r\n"
    "\r\n"
    "       header ul.list-auto li {\r\n"
    "       width: 100%;\r\n"
    "       border: solid #fff;\r\n"
    "       border-width: 0 0 1px;\r\n"
    "       display:inline-table;\r\n"
    "       }\r\n"
    "\r\n"
    "       header li a {\r\n"
    "         color: #00008b;\r\n"
    "       padding: 30px 10px;\r\n"
    "       \r\n"
    "       }\r\n"
    "     \r\n"
    "     \r\n"
    "     header li a:active {\r\n"
    "       color: #8B0000;\r\n"
    "       -moz-box-shadow:0 3px 10px 0 #000; \r\n"
    "       -webkit-box-shadow:0 3px 5px 0 #000;\r\n"
    "     }\r\n"
    "     \r\n"
    "     header ul li  ul{\r\n"
    "       position:relative; \r\n"
    "       top:0px; \r\n"
    "       left:0px;\r\n"
    "       background-color:#ccc; \r\n"
    "       //display:none;\r\n"
    "     }\r\n"
    "     \r\n"
    "     //header ul li:active ul, header li.hover ul{display:block;}\r\n"
    "       \r\n"
    "       \r\n"
    "       .control-nav { /* label icon */\r\n"
    "       position: fixed;\r\n"
    "       right: 25px;\r\n"
    "       top: 40px;\r\n"
    "       display: block;\r\n"
    "       width: 60px;\r\n"
    "       padding: 12px 0;\r\n"
    "       border: solid #fff;\r\n"
    "       border-width: 8px 0;\r\n"
    "       z-index: 2;\r\n"
    "       cursor: pointer;\r\n"
    "       }\r\n"
    "\r\n"
    "       .control-nav:before {\r\n"
    "       content: \"\";\r\n"
    "       display: block;\r\n"
    "       height: 8px;\r\n"
    "       background: #fff;\r\n"
    "       }\r\n"
    "\r\n"
    "       .control-nav-close {\r\n"
    "       position: fixed; /* label layer */\r\n"
    "       right: 0;\r\n"
    "       top: 0;\r\n"
    "       bottom: 0;\r\n"
    "       left: 0;\r\n"
    "       display: block;\r\n"
    "       z-index: 1;\r\n"
    "       background: rgba(0,0,0,0.4);\r\n"
    "       -webkit-transition: all 500ms ease;\r\n"
    "       transition: all 500ms ease;\r\n"
    "       transform: translate(100%, 0);\r\n"
    "       }\r\n"
    "\r\n"
    "       /* checked nav */\r\n"
    "       input#control-nav {\r\n"
    "       display: block;\r\n"
    "       }\r\n"
    "\r\n"
    "       input#control-nav:focus ~ .control-nav {\r\n"
    "       border-color: #000;\r\n"
    "       box-shadow: 0px 0px 9px rgba(0,0,0,0.3);\r\n"
    "       }\r\n"
    "\r\n"
    "       input#control-nav:focus ~ .control-nav:before {\r\n"
    "       background: #000;\r\n"
    "       }\r\n"
    "\r\n"
    "       input#control-nav:checked ~ nav,\r\n"
    "       input#control-nav:checked ~ .control-nav-close {\r\n"
    "       -webkit-transform: translate(0, 0);\r\n"
    "       //-ms-transform: translate(0, 0);\r\n"
    "       //transform: translate(2px, 0);\r\n"
    "       }\r\n"
    "       \r\n"
    "       table{\r\n"
    "       position: relative;\r\n"
    "       border: 0; padding: 1px 1px 1px 1px;\r\n"
    "       top: 20px;\r\n"
    "       left: 11%;\r\n"
    "       width: 70%;\r\n"
    "       height: 700px;\r\n"
    "       font-size: 45px;\r\n"
    "     }\r\n"
    "     table h2{\r\n"
    "       font-size: 45px;\r\n"
    "     }\r\n"
    "     \r\n"
    "      .switch {\r\n"
    "       width: 90px;\r\n"
    "       height: 60px;\r\n"
    "     }\r\n"
    "\r\n"
    "     .slider:before {\r\n"
    "       height: 40px;\r\n"
    "       width: 40px;\r\n"
    "       left: 4px;\r\n"
    "       bottom: 10px;\r\n"
    "     }\r\n"
    "     \r\n"
    "     input:checked + .slider:before {\r\n"
    "       transform: translateX(40px);\r\n"
    "     }\r\n"
    "     \r\n"
    "     .tooltip .tooltiptext {\r\n"
    "       /* Position the tooltip */\r\n"
    "       position: fixed;\r\n"
    "       top: -1000px;\r\n"
    "       left: 0%;\r\n"
    "       z-index: 1;\r\n"
    "     }\r\n"
    "\r\n"
    "     }\r\n"
    "\r\n"
    "     @media screen and (max-width: 480px) {\r\n"
    "       header h1 {\r\n"
    "       font-size: 20px;\r\n"
    "       line-height: 40px;\r\n"
    "       }\r\n"
    "     }\r\n"
    "     \r\n"
    "   \r\n"
    "    \r\n"
    "     </style>\r\n"
    "    </head>\r\n"
    "    <body>\r\n"
    " <header id='header'>\r\n"
    "    <div class='innertube'>\r\n"
    "    <p class='ww8'>WW8 Automatization</p>\r\n"
    "    </div>\r\n"
    "    </header>\r\n"
    " \r\n"
    " <header>\r\n"
    " <input type=\"checkbox\" id=\"control-nav\" />\r\n"
    "    <label for=\"control-nav\" class=\"control-nav\"></label>\r\n"
    "    <label for=\"control-nav\" class=\"control-nav-close\"></label>\r\n"
    " <nav class=\"float-r\">\r\n"
    "        <ul class=\"list-auto\">\r\n"
    "          <li>\r\n"
    "            <a class=\"tooltip\" href='acionar_dispositivos.html' >Dispositivos\r\n"
    "     <span class=\"tooltiptext\">Tooltip text</span>\r\n"
    "     </a>\r\n"
    "          </li>\r\n"
    "          <li>\r\n"
    "            <a> Configuração</a>\r\n"
    "     <ul>\r\n"
    "                       <li><a  class=\"tooltip\" href='configurar_wifi.html'>Configurar Wifi\r\n"
    "             <span class=\"tooltiptext\">Tooltip text</span>\r\n"
    "            </a></li>\r\n"
    "                       <li><a class=\"tooltip\" href='configurar_dispositivo.html'>Configurar Dispositivo\r\n"
    "             <span class=\"tooltiptext\">Tooltip text</span>\r\n"
    "            </a></li>\r\n"
    "            </ul>\r\n"
    "          </li>\r\n"
    "          <li>\r\n"
    "            <a class=\"tooltip\" href='ajuda.html'>Ajuda\r\n"
    "     <span class=\"tooltiptext\">Tooltip text</span>\r\n"
    "     </a>\r\n"
    "          </li>\r\n"
    "          <li>\r\n"
    "            <a class=\"tooltip\" href= 'contato.html'>Contato\r\n"
    "     <span class=\"tooltiptext\">Tooltip text</span>\r\n"
    "     </a>\r\n"
    "          </li>\r\n"
    "        </ul>\r\n"
    "      </nav>\r\n"
    " </header>\r\n";

   PROGMEM const char pos_page[] =
    "</div><footer id='footer'>\r\n"
    "<div class='innertube'>\r\n"
    "<p style='color: WHITE'>conttrole.com </p>\r\n"
    "</div>\r\n"
    "</footer>\r\n"
    "</body>\r\n"
    "</html>\r\n";
    
PROGMEM const char ajuda[] =
    "<main>\r\n"
    "<p class='cont'>Ajuda</p>\r\n"
    "<h2>Acionar Dispositivos</h2>\r\n"
    "<p>Ao ir em &quot;Dispositivos&quot; no Menu voc&ecirc; encontrar&aacute; uma lista com todos os dispositivos conectados ao SAWifi8. Os dispositivos ativos ficam marcados em azul e os n&atilde;o marcados ficam em cinza. Para acionar um dispositivo basta clicar sobre o mesmo que ele ser&aacute; acionado pelo sistema.</p>\r\n"
    "<h2>Configurar Dispositivos</h2>\r\n"
    "<p>Para configurar os dispositivos, selecionando tanto a forma de seu acionamento (se da forma <em>on-off</em> ou da forma <em>pulse</em>) quanto o seu nome, basta ir em <strong>Configura&ccedil;&atilde;o </strong>e selecionar o submenu<strong> Configurar Dispositivo</strong>.</p>\r\n"
    "<p>Para renomear basta clicar sobre o campo e alterar o seu nome, sendo permitido um m&aacute;ximo de 30 caracteres para cada dispositivo.</p>\r\n"
    "<p>Para selecionar a forma de acionamento basta clicar sobre a forma como deseja-se que seja o comportamento daquele dispositivo. O padr&atilde;o da aplica&ccedil;&atilde;o &eacute; na forma de acionamento <em>on-off</em>. Ou seja, ao clicar sobre o bot&atilde;o o dispositivo ser&aacute; ligado ou desligado. A forma de acionamento <em>pulse</em> significa que o dispositivo ao ser acionado receber&aacute; apenas um pulso de 1s (um segundo) e voltar&aacute; para o estado de desligado (sem energia), &uacute;til para acionar, por exemplo, port&otilde;es de garagens.</p>\r\n"
    "<h2>Configurar Conex&atilde;o</h2>\r\n"
    "<p>Para configurar a conex&atilde;o basta ir no menu <strong>Configura&ccedil;&otilde;es</strong> e selecionar o submenu <strong>Configurar WiFi</strong>, no qual ter&atilde;o as op&ccedil;&otilde;es de editar as configura&ccedil;&otilde;es de Access Point ou Servidor.</p>\r\n"
    "<h3>Access Point</h3>\r\n"
    "<p>No modo Access Point (modo padr&atilde;o), o dispositivo criar&aacute; uma rede no qual o usu&aacute;rio se conectar&aacute; e ter&aacute; acesso ao dispositivo atrav&eacute;s do ip <strong><em>192.168.4.1</em></strong>, a rede padr&atilde;o de f&aacute;brica &eacute;:</p>\r\n"
    "<p>Rede : <strong>rede_ww8</strong></p>\r\n"
    "<p>Senha : <strong>senha_ww8</strong></p>\r\n"
    "<p>Sugerimos que ao comprar o dispositivo o usu&aacute;rio altere a senha da rede para evitar ataques externos aos equipamentos.</p>\r\n"
    "<p>Ao selecionar a Access Point e clicar em Editar na p&aacute;gina de Configura&ccedil;&atilde;o de WiFi o usu&aacute;rio ser&aacute; redirecionado para uma p&aacute;gina onde ser&aacute; poss&iacute;vel alterar tanto a senha quanto o pr&oacute;prio nome da rede, lembrando que no modo Access Point o IP ser&aacute; sempre 192.168.4.1 para acessar o seu sistema.</p>\r\n"
    "<h3>Servidor</h3>\r\n"
    "<p>Para configurar o seu dispositivo como servidor, ou seja, conectado a uma rede j&aacute; existente e disponibilizar a p&aacute;gina somente para os usu&aacute;rios conectado &agrave;quela rede. Neste modo o sistema fica mais confi&aacute;vel pois o IP pode ser alterado e para atacar o sistema seria necess&aacute;rio inicialmente entrar na rede, ou seja, faz-se necess&aacute;rio atacar a rede E descobrir o IP selecionado pelo usu&aacute;rio.</p>\r\n"
    "<p>Ao clicar em Editar com Servidor selecionado, o usu&aacute;rio ser&aacute; redirecionado a uma p&aacute;gina onde ser&aacute; poss&iacute;vel colocar o IP, Gateway, Rede e Senha da rede a qual deseja conectar-se, para descobrir o Gateway da rede basta:</p>\r\n"
    "<p>No Windows: acessar o Prompt de Comando, digitar &quot;<em>ipconfig</em>&quot; e procurar pelo Gateway padr&atilde;o da rede.</p>\r\n"
    "<p>No Linux: acessar o Terminal, digitar &quot;<em>ifconfig</em>&quot; e procurar pelo Gateway padr&atilde;o da rede.</p>\r\n"
    "<p>No MAC: acessar o Terminal, digitar &quot;<em>route get default | grep gateway</em><code>&quot;</code> e &eacute; retornado o gateway padr&atilde;o da rede.</p>\r\n"
    "<p>Caso n&atilde;o saiba exatamente o nome da rede ou para garantir que conseguir&aacute; conectar-se &agrave; mesma pode-se ir em Selecionar Redes, ao clicar neste bot&atilde;o ser&atilde;o mostradas as 10 redes com sinal mais forte presentes nas proximidades do sistema.</p>\r\n"
    "<p>Ao selecionar a rede, digitar IP, gateway e a senha da rede basta confirmar, conectar-se &agrave; rede selecionada, digitar o IP no browser e acessar o sistema normalmente.</p>\r\n"
    "<blockquote>\r\n"
    "<p><em>Caso ocorra algum problema nas configura&ccedil;&otilde;es ou voc&ecirc; tenha esquecido IP ou senha, pode-se reinicializar o sistema para os padr&otilde;es de f&aacute;brica clicando sobre o bot&atilde;o de reset no pr&oacute;prio sistema. </em></p>\r\n"
    "</blockquote>\r\n"
    "</main>\r\n";

void index_func(){
  const char* numeric = "12345678";
  for (int i = 0; i < 8; i++){
    if(strcmp(reles[i], "Rele "+ numeric[i] +"                        ") != 0){
      acionar_dispositivo_func();
      return;
    }
  }
  configurar_dispositivo_func();
}

void configurar_wifi_func(){
    
  if(server.hasArg("wifi")){
     
     if(server.arg("wifi") == "servidor"){
   
       wifi[0] = true;
       wifi[1] = false;
       sendPage(String(ww8Obj.getServidor(rede, senha, ip, gateway).c_str()));
       
     }else if(server.arg("wifi") == "ap"){
      
       wifi[1] = true;
       wifi[0] = false;
       sendPage(String(ww8Obj.getAp(rede, senha).c_str()));
      
     }
  }else{
    sendPage(String(ww8Obj.getConfigurar_wifi(wifi).c_str()));
  }
}

void servidor_func(){
  String aux;
  String s;

  if(server.hasArg("enviar")){
    if(server.hasArg("ssid") && server.hasArg("senha")){
      aux = server.arg("ssid");
      aux.toCharArray(rede, aux.length()+1);
  
      aux = server.arg("senha");
      aux.toCharArray(senha, aux.length()+1);
  
      if(server.hasArg("ip") && server.hasArg("gateway")){
        aux = server.arg("ip");
        aux.toCharArray(ip, aux.length()+1) ;
  
        aux = server.arg("gateway");
        aux.toCharArray(gateway, aux.length()+1) ;
        
        aux.trim();
        aux.trim();
  
        s = "<html><head>"
          "<meta http-equiv='refresh' content=1;url='http://"+ String(ip) +"'>"
          "</head><body><p>Conectando-se...</p></html>";
  
        server.send(200, "text/html", s);
        
        if(aux.length() == 0){
          conectar_servidor_sem_ip(rede, senha);
        }else{
          conectar_servidor(rede, senha, ip, gateway);
        }
      }
    }
  }else if(server.hasArg("pesquisar")){
    pesquisar_wifi();
    for(int i = 0; i < 10; i++){
      aux = String(redes[i]);
      if (DEBUG) Serial.println(redes[i]);
      aux.trim();
      aux.toCharArray(redes[i],aux.length()+1);
    }
    s = String(ww8Obj.getListaRedes(title_redes, redes).c_str());
    sendPage(s);
    
  }else if(server.hasArg("selecionar")){
    aux = "";
    rede = redes[server.arg("num_rede").toInt()-1];
    aux.toCharArray(senha, 1);
    s = String(ww8Obj.getServidor(rede, senha, ip, gateway).c_str());     
    sendPage(s);  
  }else{
    s = String(ww8Obj.getServidor(rede, senha, ip, gateway).c_str());     
    sendPage(s);  
  }

  //if (DEBUG) Serial.println(s);
}

void ap_func(){
  String s;
  String aux;

  if(server.hasArg("senha") && server.hasArg("ssid")){
    
    aux = server.arg("ssid");
    aux.toCharArray(rede, aux.length()+1);
    
    aux = server.arg("senha");
    aux.toCharArray(senha, aux.length()+1);

    s = "<html><body><i>Acesse sua Rede Criada e digite o IP <b>192.168.4.1</b> para conectar-se ao ESP</i>"
    "<p>Caso n&atilde;o consiga conectar-se na rede que criaste, reset o ESP e volte &agrave;s configura&ccedil;&otilde;es iniciais com:</p>"
    "<p><b>Rede : </b> <i>rede_ww8</i></p> <p><b>Senha : </b> <i>ww8_senha</i></p></body></html>";

    sendPage(s);  
    
    conectar_ap(rede, senha);
  }
  
  s = String(ww8Obj.getAp(rede, senha).c_str());
  sendPage(s);  

  //if (DEBUG) Serial.println(s);
}

void ajuda_func(){
  int tam = String(pre_page).length() + 1;
  tam += String(ajuda).length() + 1;
  tam += String(pos_page).length() + 1;
  
  server.setContentLength(tam);
  server.send(200, "text/html", "");
  
  server.sendContent(pre_page);
  server.sendContent(ajuda);
  server.sendContent(pos_page);  
}

void acionar_dispositivo_func(){
  String rele;
  int releInt;
  if(server.hasArg("rele")){
    rele =server.arg("rele");
    releInt = rele.toInt();
    aciona(releInt);
  }
  String s = String(ww8Obj.getAcionar_dispositivo(checkeds, reles).c_str());
  sendPage(s); 
  //if (DEBUG) Serial.println(s);
}

void configurar_dispositivo_func(){
  String aux; 
  int i, val;
  bool post = false;
  String s;
  
  for (i = 0; i < 8; i++){    

    if(server.hasArg("rele_type" + String(i+1))){
      aux = server.arg("rele_type" + String(i+1));
      if(aux.compareTo("on") == 0)
        rele_type[i] = 1;
      else 
        rele_type[i] = 0;
    }

    if(server.hasArg("edit_rele" + String(i+1))){
      aux = server.arg("edit_rele" + String(i+1));
      aux.toCharArray(reles[i],aux.length()+1);
      post = true;
    }
    
    aux = String(reles[i]);
    aux.trim();
    aux.toCharArray(reles[i],aux.length()+1);
  }
  
  if(post)
    s = String(ww8Obj.getAcionar_dispositivo(checkeds, reles).c_str());
  else
    s = String(ww8Obj.getConfigurar_dispositivo(reles, rele_type).c_str());
    
  sendPage(s);  

  //if (DEBUG) Serial.println(s);
}

void contato_func(){
  String s = String(ww8Obj.getContato().c_str());
  sendPage(s);

  //if (DEBUG) Serial.println(s);
}

void aciona(int rele){
  int pin = pinos[rele - 1];
  int val = rele_type[rele - 1];
  String s;
  
  checkeds[rele - 1] = !checkeds[rele - 1];
  boolean checked = checkeds[rele - 1];

  if(val == 0){
    if(checked)
      digitalWrite(pin, HIGH);
    else
      digitalWrite(pin, LOW);
  }else{
    digitalWrite(pin, HIGH);
    delay(1000);
    digitalWrite(pin, LOW);
    checkeds[rele-1] = !checkeds[rele-1];    
    s = String(ww8Obj.getAcionar_dispositivo(checkeds, reles).c_str());
    Serial.println(s);
    sendPage(s);
  }
}

void parseBytes(char* str, char sep, byte* bytes, int maxBytes, int base) {
    for (int i = 0; i < maxBytes; i++) {
        bytes[i] = strtoul(str, NULL, base);  // Convert byte
        str = strchr(str, sep);               // Find next separator
        if (str == NULL || *str == '\0') {
            break;                            // No more separators, exit
        }
        str++;                                // Point to next character after separator
    }
}

String ipToCharArray(IPAddress myIP)
{
    String str = String(myIP[0]) + "." + String(myIP[1]) + "." + String(myIP[2]) + "." + String(myIP[3]);
    return str;
}

void conectar_ap(char* ssid, char* password){

  /* Desconecta do wifi */
  WiFi.disconnect();
  
  /* Trabalha com o modo Access Point */
  WiFi.mode(WIFI_AP);
  WiFi.hostname("ww8.");
              
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  
  if (DEBUG) Serial.print("AP IP address: ");
  if (DEBUG) Serial.println(myIP);
  
  String sip = ipToCharArray(myIP);
  sip.toCharArray(ip, sip.length() + 1);
  
  atualizar_servidor();
}

boolean conectar_servidor(char* ssid, char* password, char* ip_string, char* gateway_string){

  int WiFiCounter = 0;
  
  /* Desconecta de qualquer WiFi a qual o ESP esteja conectado */
  WiFi.disconnect();
  
  /* Trabalha com o modo Station */
  WiFi.mode(WIFI_STA);
  
  byte ip_byte[4], gateway_byte[4];
  parseBytes(ip_string, '.', ip_byte, 4, 10);
  parseBytes(gateway_string, '.', gateway_byte, 4, 10);
  
  IPAddress subnet(255, 255, 255, 0);
  WiFi.config(IPAddress(ip_byte[0],ip_byte[1],ip_byte[2],ip_byte[3]),
              IPAddress(gateway_byte[0],gateway_byte[1],gateway_byte[2],gateway_byte[3]), subnet);
  
  /* Insere nome da rede e senha para conectar-se */
  WiFi.begin(ssid, password);
  
  /* Tenta capturar IP */
   while (WiFi.status() != WL_CONNECTED && WiFiCounter < 30){
      delay(1000);
      if (DEBUG) Serial.print(".");
      WiFiCounter++;
   }
   
  /* Imprime IP capturado */
   IPAddress myIP = WiFi.localIP();
   
  String sip = ipToCharArray(myIP);
  sip.toCharArray(ip, sip.length() + 1);
  if (DEBUG) Serial.println("IP conectado : " + String(ip));
   
   String sgateway = ipToCharArray(WiFi.gatewayIP());
   sgateway.toCharArray(gateway, sgateway.length() + 1);
   if (DEBUG) Serial.println("Gateway conectado: " + String(gateway));
  
   if(WiFiCounter < 30){
    atualizar_servidor();
    return true;
   }else
    return false;
}

boolean conectar_servidor_sem_ip(char* ssid, char* password){

  int WiFiCounter = 0;
  
  /* Desconecta de qualquer WiFi a qual o ESP esteja conectado */
  WiFi.disconnect();
  
  /* Trabalha com o modo Station */
  WiFi.mode(WIFI_STA);
  
  /* Insere nome da rede e senha para conectar-se */
  WiFi.begin(ssid, password);
  
  /* Tenta capturar IP */
   while (WiFi.status() != WL_CONNECTED && WiFiCounter < 30){
      delay(1000);
      if (DEBUG) Serial.print(".");
      WiFiCounter++;
   }
  
   /* Imprime IP capturado */
   IPAddress myIP = WiFi.localIP();
   
   String sip = ipToCharArray(myIP);
   sip.toCharArray(ip, sip.length() + 1);
   if (DEBUG) Serial.println("IP conectado: " + String(ip));
   
   String sgateway = ipToCharArray(WiFi.gatewayIP());
   sgateway.toCharArray(gateway, sgateway.length() + 1);
   if (DEBUG) Serial.println("Gateway conectado: " + sgateway);
  
   if(WiFiCounter < 30){
    atualizar_servidor();
    return true;
   }else
    return false;
}

void atualizar_servidor(){
  /* Ativa o servidor para ouvir em browser a pagina onde contem os leds*/
  server.on("/", index_func);
  server.on("/acionar_dispositivos.html",acionar_dispositivo_func);
  server.on("/ajuda.html", ajuda_func);
  server.on("/contato.html", contato_func);
  server.on("/ap.html",HTTP_GET, ap_func);
  server.on("/ap.html",HTTP_POST, ap_func);
  server.on("/configurar_dispositivo.html", HTTP_GET, configurar_dispositivo_func);
  server.on("/configurar_dispositivo.html", HTTP_POST, configurar_dispositivo_func);
  server.on("/configurar_wifi.html", HTTP_GET, configurar_wifi_func);
  server.on("/configurar_wifi.html", HTTP_POST, configurar_wifi_func);
  server.on("/servidor.html", HTTP_GET, servidor_func);
  server.on("/servidor.html", HTTP_POST, servidor_func);

  /* Inicializa o servidor para ouvir */
  server.begin();
}

void pesquisar_wifi(){
  int n = WiFi.scanNetworks();
  String title_redes_aux;
  
  if(n > 10) {
    n = 10;
    title_redes_aux = "Mais de " + String(n) + " Redes Encontradas";
  }else{
    title_redes_aux = String(n) + " Redes Encontradas";
  }
 
  title_redes_aux.toCharArray(title_redes, title_redes_aux.length() + 1);

  for (int i = 0; i < n; ++i)
  {  
    WiFi.SSID(i).toCharArray(redes[i], WiFi.SSID(i).length() + 1);
  }
}

void sendPage(String page){

  int tam = String(pre_page).length() + 1;
  tam += page.length() + 1;
  tam += String(pos_page).length() + 1;
  
  server.setContentLength(tam);
  
  server.send(200, "text/html", "");
  
  server.sendContent(pre_page);

  server.sendContent(page);

  server.sendContent(pos_page);
}

