#ifndef header_h
#define header_h
#include <string>
using std::string;

class Ww8_Library {
  public:
   Ww8_Library();
   string getServidor(char* ssid, char* password, char* ip, char* gateway);
   string getContato();
   string getConfigurar_wifi(bool wifi[]);
   string getConfigurar_dispositivo(char* reles_char[], int rele_type[]);
   string getAcionar_dispositivo(bool checkeds[], char* rele_char[]);
   string getAp(char* ssid, char* password);
   string getListaRedes(char* title_redes, char* redes[]);

	string contato=
		"<main>\r\n"
		"<p class='cont'>Contatos</p>\r\n"
		"<p style='text-align: center;'>Tel:79 4141-2705(FIXO) / 79 99100-6923 (TIM)</p>\r\n"
		"<p style='text-align: center;'>Email: conttrole@conttrole.com</p>\r\n"
		"<p style='text-align: center;'>twitter: @conttrole</p>\r\n"
		"</main>\r\n";
	   
	};

#endif