#include "ww8_library.h"

Ww8_Library::Ww8_Library(void){}

string Ww8_Library::getServidor(char* ssid, char* password, char* ip, char* gateway){
	string  servidor=
		"<main>\r\n"
		"<p class='cont'>Configurar WiFi - Servidor</p>\r\n"
		"<form method='POST' action='servidor.html' >\r\n"
		"<table align='center' border='0' cellpadding='1' cellspacing='1' height='90' width='368'>\r\n"
		"<tbody>\r\n"
		"<tr><td>IP</td><td><input maxlength='20' name='ip' type='text' value='" + (string)ip + "' min='7'/></td></tr>\r\n"
		"<tr><td>Gateway</td><td><input maxlength='15' name='gateway' type='text' value='" + (string)gateway + "' min='7'/></td></tr>\r\n"
		"<tr><td>Nome da Rede</td><td><input maxlength='16' name='ssid' type='text' value='" + (string)ssid + "' min='8'/></td></tr>\r\n"
		"<tr><td>Senha</td><td><input maxlength='15' name='senha' type='password' value='" + (string)password + "' min='8'/></td></tr>\r\n"
		"<tr><td><input name='enviar' type='submit' value='Confirmar' /></td>"
		"<td><input name='pesquisar' type='submit' value='Procurar Redes Disponíveis' /></td></tr>\r\n"
		"</tbody></table>"
		"</form>\r\n"
		"</main>\r\n";
	return  servidor;
}

string Ww8_Library::getListaRedes(char* title_redes, char* redes[]){
	string table = "";
	string aux;
	string num[10] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
	for(int i = 0; i < 10; i++){
		aux = "Rede " + num[i];
		if(string(redes[i]).compare(aux) != 0){
			table += "<tr><td>" + (string)redes[i] + "</td><td><input name='num_rede' value='"+ num[i] +"' type='radio'></td></tr>";
		}else{
			break;
		}
	}
	string lista_redes = "<main>\r\n"
		"<p class='cont'>" + (string)title_redes + "</p>\r\n"
		"<form align='center' method='POST' action='servidor.html' >\r\n"
		"<table align='center' border='1' cellpadding='1' cellspacing='1' height='90' width='368'>\r\n"
		"<tbody>\r\n" + table +
		"<tr><td colspan='2'><input name='selecionar' type='submit' value='Selecionar Rede' /></td></tr>\r\n"
		"</tbody></table>"
		"</form>\r\n"
		"</main>\r\n";
		
	return  lista_redes;
}

//string  Ww8_Library::getIndex(){return index_text;}

string  Ww8_Library::getContato(){ return  contato;}

string Ww8_Library::getConfigurar_wifi(bool wifi[]){ 
	string checked = "checked";
	string vazia = "";
	string  configurar_wifi=
		"<main align='center'>\r\n"
		"<p class='cont'>Configurar WiFi</p>\r\n"
		"<form align='center' method='POST' action='configurar_wifi.html'>\r\n"
		"<p align='center'>Selecionar Tipo de Conex&atilde;o:</p>\r\n"
		"<p align='center'><input name='wifi' type='radio' value='servidor'" + (wifi[0]?checked:vazia) + "/>Servidor</p>\r\n"
		"<p align='center'><input name='wifi' type='radio' value='ap' " + (wifi[1]?checked:vazia) + "/>Access Point</p>\r\n"
		"<p align='center'><input name='enviar' type='submit' value='Editar' /></p>\r\n"
		"</form>\r\n"
		"</main>\r\n";
	return  configurar_wifi;
}

string  Ww8_Library::getConfigurar_dispositivo(char* reles[], int rele_type[]){ 
	int i;
	string checked = "checked";
	
	string configurar_dispositivo=
		"<main>\r\n"
		"<h2 style='text-align:center;'><strong>Configurar Dispositivos</strong></h2>\r\n"
		"<form method='POST' action='/configurar_dispositivo.html'>\r\n" 
		"<table align='center' border='0' cellpadding='1' cellspacing='1' height='235' width='163'>\r\n"
		"	<tbody>\r\n"
		" 		<tr><td style='text-align:center;'>Nome</td><td style='text-align:center;'>Power Pulse?</td></tr>\r\n"
		"		<tr>\r\n"
		"			<td><input maxlength='30' name='edit_rele1' type='text' value='" + (string)reles[0] + "'  /></td>\r\n"
		"			<td><input type='checkbox' name='rele_type1' " + ((rele_type[0]==1)?checked:"") + " /></td>\r\n"
		"		</tr>\r\n"
		"		<tr>\r\n"
		"			<td><input maxlength='30' name='edit_rele2' type='text' value='" + string(reles[1]) + "' /></td>\r\n"
		"			<td><input type='checkbox' name='rele_type2' " + ((rele_type[1]==1)?checked:"") + " /></td>\r\n"
		"		</tr>\r\n"
		"		<tr>\r\n"
		"			<td><input maxlength='30' name='edit_rele3' type='text' value='" + string(reles[2]) + "'  /></td>\r\n"
		"			<td><input type='checkbox' name='rele_type3' " + ((rele_type[2]==1)?checked:"") + " /></td>\r\n"
		"		</tr>\r\n"
		"		<tr>\r\n"
		"			<td><input maxlength='30' name='edit_rele4' type='text' value='" + string(reles[3]) + "'  /></td>\r\n"
		"			<td><input type='checkbox' name='rele_type4' " + ((rele_type[3]==1)?checked:"") + " /></td>\r\n"
		"		</tr>\r\n"
		"		<tr>\r\n"
		"			<td><input maxlength='30' name='edit_rele5' type='text' value='" + string(reles[4]) + "' /></td>\r\n"
		"			<td><input type='checkbox' name='rele_type5' " + ((rele_type[4]==1)?checked:"") + " /></td>\r\n"
		"		</tr>\r\n"
		"		<tr>\r\n"
		"			<td><input maxlength='30' name='edit_rele6' type='text' value='" + string(reles[5]) + "'  /></td>\r\n"
		"			<td><input type='checkbox' name='rele_type6' " + ((rele_type[5]==1)?checked:"") + " /></td>\r\n"
		"		</tr>\r\n"
		"		<tr>\r\n"
		"			<td><input maxlength='30' name='edit_rele7' type='text' value='" + string(reles[6]) + "' /></td>\r\n"
		"			<td><input type='checkbox' name='rele_type7' " + ((rele_type[6]==1)?checked:"") + " /></td>\r\n"
		"		</tr>\r\n"
		"		<tr>\r\n"
		"			<td><input maxlength='30' name='edit_rele8' type='text' value='" + string(reles[7]) + "' /></td>\r\n"
		"			<td><input type='checkbox' name='rele_type8' " + ((rele_type[7]==1)?checked:"") + " /></td>\r\n"
		"		</tr>\r\n"
		"	</tbody>\r\n"
		"</table>\r\n"
		"<div id='botao'>\r\n"
		"<input  name='button_confirm' type='submit' value='Confirmar Alterações' class='botaoEnviar' />\r\n"
		"</div>\r\n"
		"</form>\r\n"
		"</main>\r\n";
	return  configurar_dispositivo;
}

string  Ww8_Library::getAcionar_dispositivo(bool checkeds[], char* rele_char[]){
	string checked = "checked";
	string vazia = "";
	string  acionar_dispositivos=
		"<main>\r\n"
		"		<table  border=\"0\" cellpadding=\"1\" cellspacing=\"0\" height=\"368\" width=\"300\">\r\n"
		"		  <tbody>\r\n"
		"		   <tr>\r\n"
		"		     <td colspan=\"2\" style=\"text-align: center;\">\r\n"
		"		     <h2 style='text-align:center;'><strong>Acionar Dispositivos</strong></h2>\r\n"
		"		     </td>\r\n"
		"		   </tr>\r\n"
		"		   <tr>\r\n"
		"		     <td width=\"80%\" style=\"text-align: center;\">" + (string)rele_char[0] + "</td>\r\n"
		"			 <td style=\"text-align: center;\"><label class=\"switch\"> <input type=\"checkbox\" onclick=\"location.href='/adicionar_dispositivos.html?rele=1'\"" + ((checkeds[0]==1)?checked:"") + "><div class=\"slider round\"></div></label></td>\r\n"
		"		   </tr>\r\n"
		"		   <tr>\r\n"
		"		     <td style=\"text-align: center;\">" + (string)rele_char[1] + "</td>\r\n"
		"		     <td style=\"text-align: center;\"><label class=\"switch\"> <input type=\"checkbox\" onclick=\"location.href='/adicionar_dispositivos.html?rele=2'\"" + ((checkeds[1]==1)?checked:"") + "><div class=\"slider round\"></div></label></td>\r\n"
		"		   </tr>\r\n"
		"		   <tr>\r\n"
		"		     <td style=\"text-align: center;\">" + (string)rele_char[2] + "</td>\r\n"
		"		     <td style=\"text-align: center;\"><label class=\"switch\"> <input type=\"checkbox\" onclick=\"location.href='/adicionar_dispositivos.html?rele=3'\"" + ((checkeds[2]==1)?checked:"") + "><div class=\"slider round\"></div></label></td>\r\n"
		"		   </tr>\r\n"
		"		   <tr>\r\n"
		"		     <td style=\"text-align: center;\">" + (string)rele_char[3] + "</td>\r\n"
		"		     <td style=\"text-align: center;\"><label class=\"switch\"> <input type=\"checkbox\" onclick=\"location.href='/adicionar_dispositivos.html?rele=4'\"" + ((checkeds[3]==1)?checked:"") + "><div class=\"slider round\"></div></label></td>\r\n"
		"		   </tr>\r\n"
		"		   <tr>\r\n"
		"		     <td style=\"text-align: center;\">" + (string)rele_char[4] + "</td>\r\n"
		"		     <td style=\"text-align: center;\"><label class=\"switch\"> <input type=\"checkbox\" onclick=\"location.href='/adicionar_dispositivos.html?rele=5'\"" + ((checkeds[4]==1)?checked:"") + "><div class=\"slider round\"></div></label></td>\r\n"
		"		   </tr>\r\n"
		"		   <tr>\r\n"
		"		     <td style=\"text-align: center;\">" + (string)rele_char[5] + "</td>\r\n"
		"		     <td style=\"text-align: center;\"><label class=\"switch\"> <input type=\"checkbox\" onclick=\"location.href='/adicionar_dispositivos.html?rele=6'\"" + ((checkeds[5]==1)?checked:"") + "><div class=\"slider round\"></div></label></td>\r\n"
		"		   </tr>\r\n"
		"		   <tr>\r\n"
		"		     <td style=\"text-align: center;\">" + (string)rele_char[6] + "</td>\r\n"
		"		     <td style=\"text-align: center;\"><label class=\"switch\"> <input type=\"checkbox\" onclick=\"location.href='/adicionar_dispositivos.html?rele=7'\"" + ((checkeds[6]==1)?checked:"") + "><div class=\"slider round\"></div></label></td>\r\n"
		"		   </tr>\r\n"
		"		   <tr>\r\n"
		"		     <td style=\"text-align: center;\">" + (string)rele_char[7] + "</td>\r\n"
		"		     <td style=\"text-align: center;\"><label class=\"switch\"> <input type=\"checkbox\" onclick=\"location.href='/adicionar_dispositivos.html?rele=8'\"" + ((checkeds[7]==1)?checked:"") + "><div class=\"slider round\"></div></label></td>\r\n"
		"		   </tr>\r\n"
		"		 </tbody>\r\n"
		"		</table>\r\n"
		"		\r\n"
		"	</main>\r\n";
		
	return  acionar_dispositivos;
}

string  Ww8_Library::getAp(char* ssid, char* password){
	string ap=
		"<main>\r\n"
		"<p class='cont'>Configurar WiFi - Access Point</p>\r\n"
		"<form method='POST' action='ap.html'>\r\n"
		"<table align='center' border='0' cellpadding='1' cellspacing='1' height='90' width='368'>\r\n"
		"	<tbody>\r\n"
		"		<tr>\r\n"
		"			<td>Nome da Rede :</td>\r\n"
		"			<td><input maxlength='16' name='ssid' type='text' value=\'" + ((string)ssid) + "' /></td>\r\n"
		"		</tr>\r\n"
		"		<tr>\r\n"
		"			<td>Senha :</td>\r\n"
		"			<td><input name='senha' type='password' value= '" + ((string)password) + "'/></td>\r\n"
		"		</tr>\r\n"
		"		<tr>\r\n"
		"			<td colspan='2' style='text-align: center;'><input name='enviar' type='submit' value='Enviar' /></td>\r\n"
		"		</tr>\r\n"
		"	</tbody>\r\n"
		"</table>\r\n"
		"</form>\r\n"
		"</main>\r\n";
	return  ap;
}